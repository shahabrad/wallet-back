package wallet.back.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class EmailComponent {
    @Autowired
    private JavaMailSender emailSender;

    @Value("${app.mail.from}")
    private String from;


    public void sendEmail(String to, String subject, String content) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message);
        messageHelper.setSubject(subject);
        messageHelper.setText(content);
        messageHelper.setTo(to);
        messageHelper.setFrom(from);
        emailSender.send(message);
    }
}
