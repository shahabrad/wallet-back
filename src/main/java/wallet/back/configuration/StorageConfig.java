package wallet.back.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageConfig {
    @Value("${storage.upload.folder}")
    String path;

    public String getAvatarsPath() {
        return path + "/avatar";
    }

    public String getReviewPath() {
        return path + "/review";
    }
}
