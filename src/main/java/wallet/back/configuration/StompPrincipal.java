package wallet.back.configuration;

import wallet.back.domain.entity.User;

import java.security.Principal;

public class StompPrincipal implements Principal {

    private String name;
    private User user;

    public StompPrincipal(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}

