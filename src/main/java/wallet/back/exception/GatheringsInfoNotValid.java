package wallet.back.exception;

public class GatheringsInfoNotValid extends Exception {
    public GatheringsInfoNotValid() {
        super("Gathering's info not valid");
    }
}
