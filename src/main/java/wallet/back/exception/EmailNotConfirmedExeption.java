package wallet.back.exception;

public class EmailNotConfirmedExeption extends Exception {
    public EmailNotConfirmedExeption() {
        super("email not confirmed");
    }
}
