package wallet.back.exception;

public class EmailNotExistsException extends Exception {
    public EmailNotExistsException() {
        super("email not found");
    }
}
