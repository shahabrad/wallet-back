package wallet.back.exception;

public class TokenIsNotValidException extends Exception {
    public TokenIsNotValidException() {
        super("Token is not valid");
    }
}
