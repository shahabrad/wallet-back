package wallet.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import wallet.back.configuration.CheckGroupAvailabilityInterceptor;
import wallet.back.domain.dto.LoginDto;
import wallet.back.domain.dto.MessageDto;
import wallet.back.domain.dto.SignupDto;
import wallet.back.domain.dto.TokenDto;
import wallet.back.domain.entity.User;
import wallet.back.exception.EmailExistsException;
import wallet.back.exception.EmailNotConfirmedExeption;
import wallet.back.exception.UserNotFoundException;
import wallet.back.service.UserService;
import wallet.back.util.JwtToken;

import javax.mail.MessagingException;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/wallet/api/authenticate")
public class AuthenticationController {

    @Autowired
    public JwtToken jwtToken;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Value("${test.email}")
    private String testEmail;


    public void init() {
        CheckGroupAvailabilityInterceptor.authenticationController = this;
    }

    private void authenticate(String email, String password) throws BadCredentialsException, DisabledException {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
    }

    @PostMapping("/login")
    public ResponseEntity<TokenDto> createToken(@RequestBody LoginDto loginDto) throws Exception {
        authenticate(loginDto.getEmail(), loginDto.getPassword());
        final UserDetails userDetails = userDetailsService.loadUserByUsername(loginDto.getEmail());
        User user = userService.findByEmail(loginDto.getEmail()).orElseThrow(UserNotFoundException::new);
        final String token = jwtToken.generateToken(userDetails);
        if (!user.isActive() && !user.getEmail().trim().equals(testEmail)) {
            throw new EmailNotConfirmedExeption();
        } else {
            return ResponseEntity.ok(new TokenDto(token));
        }
    }

    @PostMapping("/register")
    public ResponseEntity<MessageDto> registerUser(@RequestBody SignupDto signupDto) throws EmailExistsException, MessagingException {
        if (!signupDto.isValid())
            return new ResponseEntity<>(MessageDto.builder().message("bad request").build(), HttpStatus.BAD_REQUEST);
        userService.registerUser(signupDto);
        return ResponseEntity.ok(MessageDto.builder().message("Successfully registered").build());
    }
}
