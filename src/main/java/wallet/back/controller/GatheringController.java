package wallet.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import wallet.back.configuration.StompPrincipal;
import wallet.back.domain.dto.*;
import wallet.back.domain.entity.Gathering;
import wallet.back.domain.entity.Message;
import wallet.back.domain.entity.Profile;
import wallet.back.exception.GatheringNotFoundException;
import wallet.back.exception.GatheringsInfoNotValid;
import wallet.back.service.GatheringService;
import wallet.back.service.ProfileService;
import wallet.back.service.UserDetailsServiceImpl;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/wallet/api/gathering")
public class GatheringController {

    @Autowired
    GatheringService gatheringService;

    @Autowired
    ProfileService profileService;

    @Autowired
    UserDetailsServiceImpl detailsService;

    @Autowired
    private SimpMessagingTemplate template;


    @PostMapping
    public ResponseEntity<CreateGatheringDto> createGathering(@RequestBody CreateGatheringDto gatheringDto) throws GatheringsInfoNotValid {
        if (!gatheringDto.isValid())
            throw new GatheringsInfoNotValid();
        Gathering gathering = gatheringService.createGathering(gatheringDto, detailsService.getUser().getProfile());

        return ResponseEntity.ok((CreateGatheringDto) new CreateGatheringDto().toDto(gathering));
    }

    @GetMapping("/getGatherings")
    public ResponseEntity<List<GatheringDto>> getGatherings() {
        List<Gathering> gatherings = profileService.getGatheringsByProfile(detailsService.getUser().getProfile());

        List<GatheringDto> gatheringDtos = gatherings.stream()
                .map(gathering -> (GatheringDto) new GatheringDto().toDto(gathering)).collect(Collectors.toList());

        return ResponseEntity.ok(gatheringDtos);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MessageDto> inviteUser(@RequestBody InviteUserToGatheringDto gatheringDto, @PathVariable String id) throws GatheringsInfoNotValid, GatheringNotFoundException {
        if (!gatheringDto.isValid())
            throw new GatheringsInfoNotValid();

        gatheringDto.extractIdentifier();
        gatheringService.inviteUserToGathering(gatheringDto, id, detailsService.getUser().getProfile());
        return ResponseEntity.ok(MessageDto.builder().message("invited").build());
    }

    @GetMapping("/members/{id}")
    public ResponseEntity<List<MemberCardDto>> getMembers(@PathVariable String id) throws GatheringNotFoundException {
        List<Profile> profiles = gatheringService.getProfiles(id);
        List<MemberCardDto> memberCardDtos = profiles.stream().map(profile -> (MemberCardDto) new MemberCardDto().toDto(profile)).collect(Collectors.toList());
        return ResponseEntity.ok(memberCardDtos);
    }

    @GetMapping("/history/{id}")
    public ResponseEntity<List<ChatMessageDto>> getGatheringHistory(@PathVariable String id) throws GatheringNotFoundException {
        List<ChatMessageDto> collect = gatheringService.getGatheringMessages(id).stream().sorted(Comparator.comparingLong(Message::getDate))
                .map(message -> ((ChatMessageDto) new ChatMessageDto().toDto(message))).collect(Collectors.toList());
        return ResponseEntity.ok(collect);
    }

    @MessageMapping("/gathering/{id}")
    public void broadcast(@DestinationVariable("id") String id, ChatMessageDto messageDto, StompPrincipal principal) throws GatheringNotFoundException {
        Message message = gatheringService.saveMessage(messageDto, principal.getUser().getProfile(), id);
        template.convertAndSend("/topic/gathering/" + id, messageDto.toDto(message));
    }
}
