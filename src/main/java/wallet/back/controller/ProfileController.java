package wallet.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import wallet.back.domain.dto.ProfileInfoDto;
import wallet.back.domain.entity.Profile;
import wallet.back.exception.UserNotFoundException;
import wallet.back.service.ProfileService;
import wallet.back.service.UserDetailsServiceImpl;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/wallet/api/profile")
public class ProfileController {

    @Autowired
    ProfileService profileService;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @PostMapping()
    public ResponseEntity<ProfileInfoDto> updateProfile(@RequestBody ProfileInfoDto profileInfoDto) throws Exception {
        if (!profileInfoDto.isValid())
            throw new Exception(); // todo: proper error

        Profile profile = profileService.updateProfile(profileInfoDto, userDetailsService.getUser().getId().toString());

        return ResponseEntity.ok((ProfileInfoDto) new ProfileInfoDto().toDto(profile));
    }

    @GetMapping()
    public ResponseEntity<ProfileInfoDto> getProfile() throws UserNotFoundException {
        return ResponseEntity.ok((ProfileInfoDto) new ProfileInfoDto()
                .toDto(profileService.getProfileByUserId(userDetailsService.getUser().getId().toString())));
    }
}
