package wallet.back.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import wallet.back.domain.entity.Message;

import java.util.Date;
import java.util.UUID;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class ChatMessageDto implements IInputDto<Message>, IOutputDto<Message> {

    String id;
    String gathering;
    String message;
    String from;
    long date;

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public Message fromDto() {
        return Message.builder().id(UUID.randomUUID()).message(this.message).date(new Date().getTime()).build();
    }

    @Override
    public Message updateDto(Message message) {
        return null;
    }

    @Override
    public IOutputDto<Message> toDto(Message message) {
        return ChatMessageDto.builder()
                .from(message.getProfile().getUser().getEmail())
                .message(message.getMessage())
                .date(message.getDate())
                .id(message.getId().toString())
                .gathering(message.getGathering().getId().toString())
                .build();
    }
}
