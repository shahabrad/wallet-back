package wallet.back.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import wallet.back.domain.entity.User;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InviteNewUserDto implements IInputDto<User> {

    String email;
    String from;


    @Override
    public boolean isValid() {
        return email.contains("@") && !from.isEmpty();
    }

    @Override
    public User fromDto() {
        return User.builder().email(this.email).active(false).build();
    }

    @Override
    public User updateDto(User user) {
        return user.toBuilder().email(this.email).build();
    }
}
