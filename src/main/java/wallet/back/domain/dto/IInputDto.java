package wallet.back.domain.dto;

public interface IInputDto<T> {
    boolean isValid();

    T fromDto();

    T updateDto(T t);
}
