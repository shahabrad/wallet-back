package wallet.back.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import wallet.back.domain.entity.Gathering;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GatheringDto implements IOutputDto<Gathering> {

    String id;
    String title;
    String avatar;


    @Override
    public IOutputDto<Gathering> toDto(Gathering gathering) {
        return GatheringDto.builder()
                .avatar(gathering.getAvatar())
                .title(gathering.getTitle())
                .id(gathering.getId().toString())
                .build();
    }
}
