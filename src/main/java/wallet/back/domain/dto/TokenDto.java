package wallet.back.domain.dto;

import lombok.Data;

@Data
public class TokenDto {

    final String token;


}
