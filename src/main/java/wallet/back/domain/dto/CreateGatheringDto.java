package wallet.back.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import wallet.back.domain.entity.Gathering;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class CreateGatheringDto implements IInputDto<Gathering>, IOutputDto<Gathering> {

    String title;
    String avatar;
    String id;

    @Override
    public boolean isValid() {
        return title != null && !title.isEmpty();
    }

    @Override
    public Gathering fromDto() {
        return Gathering.builder().title(this.title).avatar(this.avatar).build();
    }

    @Override
    public Gathering updateDto(Gathering gathering) {
        return gathering.toBuilder().title(this.title).avatar(this.avatar).build();
    }

    @Override
    public IOutputDto<Gathering> toDto(Gathering gathering) {
        return CreateGatheringDto.builder()
                .title(gathering.getTitle())
                .avatar(gathering.getAvatar())
                .id(gathering.getId().toString())
                .build();
    }
}
