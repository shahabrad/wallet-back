package wallet.back.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import wallet.back.domain.entity.Profile;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfileInfoDto implements IInputDto<Profile>, IOutputDto<Profile> {

    String firstName;
    String lastName;
    String avatar;
    String username;

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public Profile fromDto() {
        return Profile.builder().firstName(this.firstName).lastName(this.lastName).avatar(this.avatar).build();
    }

    @Override
    public Profile updateDto(Profile profile) {
        return profile.toBuilder().firstName(this.firstName).lastName(this.lastName).avatar(this.avatar).build();
    }

    @Override
    public IOutputDto<Profile> toDto(Profile profile) {
        return ProfileInfoDto.builder()
                .firstName(profile.getFirstName())
                .lastName(profile.getLastName())
                .username(profile.getUser().getUsername())
                .avatar(profile.getAvatar())
                .build();
    }
}
