package wallet.back.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import wallet.back.domain.entity.Book;
import wallet.back.domain.entity.Review;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReviewDto implements IInputDto<Review>, IOutputDto<Review> {

    String author;
    String title;
    String buyUrl;
    String review;
    String publisher;
    String fileName;

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public Review fromDto() {
        return Review.builder().review(review)
                .book(Book.builder()
                        .author(author.trim().toLowerCase())
                        .publisher(publisher.trim().toLowerCase())
                        .title(title.trim().toLowerCase()).build())
                .buyUrl(buyUrl).build();
    }

    @Override
    public Review updateDto(Review review) {
        return review;
    }

    @Override
    public IOutputDto<Review> toDto(Review review) {
        return ReviewDto.builder()
                .author(review.getBook().getAuthor())
                .publisher(review.getBook().getPublisher())
                .buyUrl(review.getBuyUrl())
                .review(review.getReview())
                .title(review.getBook().getTitle())
                .fileName(review.getFileName())
                .build();

    }
}
