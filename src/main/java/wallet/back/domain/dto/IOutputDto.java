package wallet.back.domain.dto;

public interface IOutputDto<T> {
    IOutputDto<T> toDto(T t);
}
