package wallet.back.domain.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Profile {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Type(type = "uuid-char")
    private UUID id;

    @ManyToMany(mappedBy = "profiles", fetch = FetchType.EAGER)
    private List<Gathering> gatherings;

    @OneToOne
    private User user;

    private String firstName;
    private String lastName;
    private String avatar;

}
