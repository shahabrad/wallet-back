package wallet.back.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Gathering {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Type(type = "uuid-char")
    private UUID id;

    private String title;

    private String avatar;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable(
            name = "profile_gathering",
            joinColumns = {@JoinColumn(name = "gathering_id")},
            inverseJoinColumns = {@JoinColumn(name = "profile_id")}
    )
    private List<Profile> profiles;

    @OneToMany(mappedBy = "gathering")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Message> messages;

}
