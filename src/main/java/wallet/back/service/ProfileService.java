package wallet.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wallet.back.domain.dto.ProfileInfoDto;
import wallet.back.domain.entity.Gathering;
import wallet.back.domain.entity.Profile;
import wallet.back.exception.UserNotFoundException;
import wallet.back.repository.ProfileRepository;

import java.util.List;
import java.util.UUID;

@Service
public class ProfileService {

    @Autowired
    ProfileRepository profileRepository;

    public Profile updateProfile(ProfileInfoDto profileInfoDto, String userId) throws UserNotFoundException {
        Profile profile = profileRepository.findByUserId(UUID.fromString(userId)).orElseThrow(UserNotFoundException::new);
        profile = profileInfoDto.updateDto(profile);
        if (profileInfoDto.getUsername() != null)
            profile.getUser().setUsername(profileInfoDto.getUsername());
        profileRepository.save(profile);
        return profile;
    }

    public Profile getProfileByUserId(String userId) throws UserNotFoundException {
        return profileRepository.findByUserId(UUID.fromString(userId)).orElseThrow(UserNotFoundException::new);
    }

    public List<Gathering> getGatheringsByProfile(Profile profile) {
        return profileRepository.findById(profile.getId()).get().getGatherings();
    }
}
