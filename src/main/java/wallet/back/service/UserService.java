package wallet.back.service;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import wallet.back.component.EmailComponent;
import wallet.back.domain.dto.InviteNewUserDto;
import wallet.back.domain.dto.SignupDto;
import wallet.back.domain.entity.Profile;
import wallet.back.domain.entity.User;
import wallet.back.domain.entity.VerificationToken;
import wallet.back.exception.EmailExistsException;
import wallet.back.exception.EmailNotExistsException;
import wallet.back.exception.TokenIsNotValidException;
import wallet.back.repository.UserRepository;
import wallet.back.repository.VerificationTokenRepository;

import javax.mail.MessagingException;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    VerificationTokenRepository verificationTokenRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private EmailComponent emailComponent;

    @Value("${wallet.user_generation_link.base_url}")
    private String INITIAL_URL;

    public void registerUser(SignupDto signupDto) throws EmailExistsException, MessagingException {
        boolean exists = userRepository.existsByEmail(signupDto.getEmail().trim().toLowerCase());
        if (exists) throw new EmailExistsException();

        User user = User.builder()
                .email(signupDto.getEmail().toLowerCase())
                .password(encoder.encode(signupDto.getPassword()))
                .active(false).build();

        VerificationToken verificationToken = verificationTokenRepository.findByEmail(signupDto.getEmail())
                .orElse(VerificationToken.builder()
                        .email(signupDto.getEmail().trim().toLowerCase())
                        .token(UUID.randomUUID().toString())
                        .build());

        StringBuilder sb = new StringBuilder()
                .append("Hi dear user, click the link to confirm\n")
                .append(INITIAL_URL)
                .append("/user/activate/")
                .append(verificationToken.getToken());

        emailComponent.sendEmail(signupDto.getEmail().trim(), "wallet co. confirmation mail", sb.toString());
        verificationTokenRepository.save(verificationToken);
        userRepository.save(user);
    }

    @SneakyThrows
    public void inviteUser(InviteNewUserDto userDto) {
        StringBuilder sb = new StringBuilder()
                .append("Hi, your friend ")
                .append(userDto.getFrom())
                .append(" has invited you to a group in Wallet's site")
                .append(", you can use this link to register\n")
                .append(INITIAL_URL)
                .append("/authenticate/register");

        emailComponent.sendEmail(userDto.getEmail(), "wallet co. invitation mail", sb.toString());
    }

    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User activateUser(String token) throws TokenIsNotValidException, EmailNotExistsException {
        VerificationToken verificationToken = verificationTokenRepository.findByToken(token).orElseThrow(TokenIsNotValidException::new);
        User user = userRepository.findByEmail(verificationToken.getEmail()).orElseThrow(EmailNotExistsException::new);
        user.setActive(true);
        if (user.getProfile() == null)
            user.setProfile(Profile.builder().user(user).build());
        userRepository.save(user);
        return user;
    }

    public boolean checkUsername(String username) {
        return userRepository.findByUsername(username).isPresent();
    }
}
