package wallet.back.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.nio.file.Path;

public interface IStorageService {
    void init();

    String storeAvatar(MultipartFile file);

    String storeReview(MultipartFile file);

    Path loadAvatar(String fileName);

    Path loadReview(String fileName);

    Resource loadAvatarAsResource(String fileName) throws FileNotFoundException;

    Resource loadReviewAsResource(String fileName) throws FileNotFoundException;
}
