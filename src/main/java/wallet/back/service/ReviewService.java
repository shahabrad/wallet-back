package wallet.back.service;

import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import wallet.back.domain.dto.ReviewDto;
import wallet.back.domain.entity.Book;
import wallet.back.domain.entity.Profile;
import wallet.back.domain.entity.Review;
import wallet.back.repository.BookRepository;
import wallet.back.repository.ReviewRepository;

import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

@Service
public class ReviewService {
    @Autowired
    ReviewRepository reviewRepository;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    ImageStorageService storageService;

    @Value("${review.file.path}")
    String reviewFilePath;


    public Review addReview(ReviewDto reviewDto, Profile profile) throws Exception {
        Book book = bookRepository.findByTitle(reviewDto.getTitle().trim().toLowerCase())
                .orElse(reviewDto.fromDto().getBook().toBuilder().id(UUID.randomUUID()).build());

        Review build = reviewDto.fromDto().toBuilder().book(book).profile(profile).fileName(buildPhoto(reviewDto)).build();

        bookRepository.save(book);
        reviewRepository.save(build);
        return build;
    }

    private String buildPhoto(ReviewDto reviewDto) throws TranscoderException, IOException {
        Scanner scanner = new Scanner(new FileInputStream(reviewFilePath));
        StringBuilder sb = new StringBuilder();
        String[] lines = getLines(reviewDto.getReview());
        while (scanner.hasNextLine()) sb.append(scanner.nextLine()).append("\n");
        String replace = sb.toString().replace("title", reviewDto.getTitle())
                .replace("line1", lines[0]).replace("line2", lines[1]).replace("line3", lines[2]);
        InputStream stream = new ByteArrayInputStream(replace.getBytes());

        TranscoderInput input = new TranscoderInput(stream);
        String fileName = new Date().getTime() + ".png";
        OutputStream outputStream = new FileOutputStream(storageService.reviewPath + "/" + fileName);
        TranscoderOutput output = new TranscoderOutput(outputStream);
        PNGTranscoder transcoder = new PNGTranscoder();
        transcoder.transcode(input, output);
        outputStream.flush();
        outputStream.close();
        return fileName;
    }

    private String[] getLines(String review) {
        String[] words = review.trim().split(" ");
        String[] lines = new String[3];
        for (int i = 0; i < 3; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < 6 && j + i * 6 < words.length; j++) sb.append(words[j + i * 6]).append(" ");
            lines[i] = sb.toString().isEmpty() ? ":)" : sb.toString();
        }
        return lines;
    }

    public List<Review> getReviews(Profile profile) {
        return reviewRepository.findByProfileId(profile.getId());
    }
}
