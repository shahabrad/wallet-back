package wallet.back.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import wallet.back.domain.entity.Book;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface BookRepository extends CrudRepository<Book, String> {
    Optional<Book> findById(UUID id);

    Optional<Book> findByTitle(String title);

}
