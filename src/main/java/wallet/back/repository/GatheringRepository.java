package wallet.back.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import wallet.back.domain.entity.Gathering;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface GatheringRepository extends CrudRepository<Gathering, String> {
    Optional<Gathering> findById(UUID id);
}
